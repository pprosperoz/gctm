//
//  File.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation
import UIKit

struct RouteModel {
    var name: String?
    var image: Data?
    var distance: Double?
    var time: String?
}
