//
//  ListRoutesPresenter.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation
import UIKit

protocol ListRoutesPresenterProtocol {

    var routes: [Route]? { get }
    var numberOfRows: Int { get }
    func cellInformation(index: Int) -> Route?
    func configureCell(index: Int, cell: UITableViewCell?) -> UITableViewCell

}

protocol ListRoutesViewProtocol {
    func updateView()
}

class ListRoutesPresenter: ListRoutesPresenterProtocol {

    var routes: [Route]? {
        return CoreDataHelper.shared.routes()
    }
    var numberOfRows: Int {
        return routes?.count ?? 0
    }
    private var view: ListRoutesViewProtocol

    init(view: ListRoutesViewProtocol) {
        self.view = view
        view.updateView()
    }

    func cellInformation(index: Int) -> Route? {
        guard let info = routes?[safe: index] else {
            return nil
        }
        return info
    }

    func configureCell(index: Int, cell: UITableViewCell?) -> UITableViewCell {
        guard let info = routes?[safe: index], let cell = cell else {
            return UITableViewCell()
        }
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = info.nameRoute
        cell.textLabel?.sizeToFit()
        cell.accessoryType = .disclosureIndicator

        return cell
    }

}
