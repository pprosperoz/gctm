//
//  RouteViewPresenter.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation

protocol RoutePresenterProtocol {
    var routeModel: RouteModel { get set }
    var timeStarted: Date { get set }
    func saveRoute()
    func setTrackingTime()
}

protocol RouteViewProtocol {
    func clearRoute()
}

class RouteViewPresenter: RoutePresenterProtocol {
    var timeStarted: Date
    var routeModel: RouteModel = RouteModel()
    private var view: RouteViewProtocol

    init(view: RouteViewProtocol) {
        self.view = view
        timeStarted = Date()
    }

    func saveRoute() {
        CoreDataHelper.shared.save(routeModel: routeModel, completion: {result,message in
            self.view.clearRoute()
        })
    }

    func setTrackingTime() {
        let interval = Date().timeIntervalSince(timeStarted)
        routeModel.time = interval.stringFromTimeInterval()
    }
}
