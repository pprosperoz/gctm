//
//  DetailsPresenter.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation
import UIKit
import Social

protocol DetailsPresenterProtocol {

    var details: Route? { get }
    func shareFB()
    func shareWA()

}

protocol DetailsViewProtocol {
//    func share(object: ShareModel, for serviceType: String)
}

class DetailsPresenter: DetailsPresenterProtocol {

    private var view: DetailsViewProtocol
    var details: Route?

    init(details: Route, view: DetailsViewProtocol) {
        self.details = details
        self.view = view
    }

    func shareFB() {
    }

    func shareWA() {
    }
}
