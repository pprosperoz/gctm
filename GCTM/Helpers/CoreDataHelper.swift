//
//  CoreDataHelper.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation
import UIKit
import CoreData

typealias operationCompleted = ((_ deleted: Bool,_ message: String) -> Void)

class CoreDataHelper {

    static let shared = CoreDataHelper()
    private init() {}

    lazy var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "CGTMDB")


        container.loadPersistentStores(completionHandler: { (storeDescription, error) in

            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    func saveContext () {
        let context = CoreDataHelper.shared.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func save(routeModel: RouteModel, completion: @escaping operationCompleted) {
        let managedContext = CoreDataHelper.shared.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Route",
                                                in: managedContext)!
        let aRoute = NSManagedObject(entity: entity,
                                     insertInto: managedContext)

        let id = UUID().uuidString

        aRoute.setValue(id, forKey: "id")
        aRoute.setValue(routeModel.distance, forKeyPath: "distance")
        aRoute.setValue(routeModel.time, forKeyPath: "time")
        aRoute.setValue(routeModel.name, forKeyPath: "nameRoute")
        aRoute.setValue(routeModel.image, forKey: "routeImg")
        do {
            try managedContext.save()
            completion(true, "New Route Added")
        } catch let error as NSError {
            completion(false, "Couldn't save route")
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    func routes() -> [Route]?{
        let managedContext = CoreDataHelper.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Route")
        do {
            let routes = try managedContext.fetch(fetchRequest)
            return routes as? [Route]
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }

    func delete(id: String, completion: @escaping operationCompleted) {
        let managedContext = CoreDataHelper.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Route")
        fetchRequest.predicate = NSPredicate(format: "id == %@" ,id)
        do {
            let item = try managedContext.fetch(fetchRequest)
            var removed = [Route]()
            for i in item {
                managedContext.delete(i)
                try managedContext.save()
                removed.append(i as! Route)
            }
            if removed.count > 0 {
                completion(true, "Removed")
            }
            completion(false, "Couldn't remove")
        } catch let error as NSError {
            print(error)
            completion(false, "Couldn't remove")
        }
    }
}

