//
//  Constants.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    struct RouteView {
        static let startTitle = "Start Route"
        static let stopTitle = "Recording..."
        static let playIconName = "livephoto.play"
        static let stopIconName = "stop.circle"
        static let saveAlertTitle = "SAVE ROUTE"
        static let saveAlertMessage = "Please enter a name for your route"
        static let routeNamePlaceholder = "Route name"
        static let alertSaveButtonTitle = "Save"
        static let alertDiscardButtonTitle = "Discard"
        static let locationAlertTitle = "Location Services"
        static let locationAlertMessage = "Please enable Location Services in Settings"
        static let acceptTitle = "Accept"
        static let defaultNameRoute = "Route"
    }

    struct HomeView {
        static let title = "Tracking route"
    }

    struct DetailsView {
        static let title = "Details"
        static let timeTravelText = "TIME TRAVEL:"
        static let distanceText = "DISTANCE IN KM:"
    }

    struct appColors {
        static let customBlue = UIColor(red: 6.0/255.0, green: 69.0/255.0, blue: 173.0/255.0, alpha: 1.0)
    }
}
