//
//  UIAlertController+Extension.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/24/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
  func addActions(_ actions: [UIAlertAction], preferred: String? = nil) {

    for action in actions {
      self.addAction(action)

      if let preferred = preferred, preferred == action.title {
        self.preferredAction = action
      }
    }
  }
}
