//
//  TimeInterval+Extension.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation

extension TimeInterval{

func stringFromTimeInterval() -> String {

    let time = NSInteger(self)

    let seconds = time % 60
    let minutes = (time / 60) % 60
    let hours = (time / 3600)

    var formatString = ""
    if hours == 0 {
        if(minutes < 10) {
            formatString = "%2d min, %0.2d sec"
        }else {
            formatString = "%0.2d min, %0.2d sec"
        }
        return String(format: formatString,minutes,seconds)
    }else {
        formatString = "%2d hours, %0.2d min, %0.2d sec"
        return String(format: formatString,hours,minutes,seconds)
    }
}
}
