//
//  ViewController+Extension.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation
import UIKit

typealias action = () -> Void
typealias fieldBehavior = (_ field: UITextField) -> Void

extension UIViewController {

    func getViewController<T>(viewController:T.Type, storyboardName:String) -> T? where T: UIViewController {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        let identifier = String(describing: T.self)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: identifier) as? T else {
            return nil
        }
        return vc
    }

    public class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }

    func addDetailsButton(action: Selector) {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: action)
    }

    func addBackButton() {
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(self.backAction))
        }
    }

    @objc private func backAction() {
        self.navigationController?.popViewController(animated: true)
    }


    func presentAlert(_ title: String,
                      body: String?,
                      includeField: Bool = false,
                      entryTextFieldAction: @escaping fieldBehavior = { _ in },
                      firstTitle: String? = nil,
                      firstAction: action? = nil,
                      secondTitle: String? = nil,
                      secondAction: action? = nil,
                      defaultButton: Bool = false,
                      cancelButtonTitle: String = "Cancel") {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)

        if includeField {
            alertController.addTextField(configurationHandler: { (textfield) in
                entryTextFieldAction(textfield)
            })
        }

        if let firstTitle = firstTitle, let action = firstAction {
            let firstAction = UIAlertAction(title: firstTitle, style: .default) { (_) in
                action()
            }
            alertController.addAction(firstAction)
        }

        if let secondTitle = secondTitle, let action = secondAction {
            let secondAction = UIAlertAction(title: secondTitle, style: .default, handler: { (_) in
                action()
            })
            alertController.addAction(secondAction)
        }

        if defaultButton {
            let cancel = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil)
            alertController.addAction(cancel)
        }

        present(alertController, animated: true, completion: nil)
    }
}
