//
//  Double+Extension.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/23/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation

extension Double {
    func toString() -> String {
        return String(format: "%.2f",self)
    }
}
