//
//  RouteViewController.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/22/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

enum StatusRoute: Int {
    case drawingIsStopped
    case drawingIsRunning
}

class RouteViewController: UIViewController {

    @IBOutlet weak var startSaveRouteButton: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    private var locationManager = CLLocationManager()
    private var path = GMSMutablePath()
    private var polyline = GMSPolyline()
    private var initialPositionMarker = GMSMarker()
    private var currentPositionMarker = GMSMarker()
    private var drawingStatus: StatusRoute = .drawingIsStopped
    private var nameTextField: UITextField?
    var presenter: RoutePresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationManager()
        setupView()
        styleButton()
        presenter = RouteViewPresenter(view: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    // MARK: - HELPER METHODS
    private func setupView() {
        nameTextField = UITextField()
        startSaveRouteButton.layer.borderWidth = 2
        startSaveRouteButton.layer.cornerRadius = 5
     }

    private func setupLocationManager() {
        locationManager.delegate = self
        self.setupPolylineAnnotation()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    private func setupPolylineAnnotation() {
        self.polyline.strokeColor = UIColor.blue
        self.polyline.strokeWidth = 5.0
        self.polyline.map = self.mapView
    }

    private func styleButton() {
        if drawingStatus == .drawingIsStopped {
            startSaveRouteButton.layer.borderColor = Constants.appColors.customBlue.cgColor
            startSaveRouteButton.setTitle(Constants.RouteView.startTitle, for: .normal)
            if #available(iOS 13.0, *) {
                let playImage = UIImage(systemName: Constants.RouteView.playIconName)
                startSaveRouteButton.setImage(playImage, for: .normal)
            }

            startSaveRouteButton.setTitleColor(Constants.appColors.customBlue, for: .normal)
            startSaveRouteButton.tintColor = Constants.appColors.customBlue
        } else {
            startSaveRouteButton.layer.borderColor = UIColor.red.cgColor
            startSaveRouteButton.setTitle(Constants.RouteView.stopTitle, for: .normal)
            if #available(iOS 13.0, *) {
                let stopImage = UIImage(systemName: Constants.RouteView.stopIconName)
                startSaveRouteButton.setImage(stopImage, for: .normal)
            }
            startSaveRouteButton.setTitleColor(.red, for: .normal)
            startSaveRouteButton.tintColor = .red
        }
    }

    func confirmView() {
        presentAlert(Constants.RouteView.saveAlertTitle,
                     body: Constants.RouteView.saveAlertMessage,
                     includeField: true,

                     entryTextFieldAction: { (field) in
                        self.nameTextField = field
                        self.nameTextField?.placeholder = Constants.RouteView.routeNamePlaceholder
        },
                     firstTitle: Constants.RouteView.alertDiscardButtonTitle,
                     firstAction: { self.clearRoute() },
                     secondTitle: Constants.RouteView.alertSaveButtonTitle,
                     secondAction: { self.saveRoute() })
    }
    
    @IBAction func startSaveRouteAction(_ sender: Any) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
                return
            case .restricted, .denied:
                presentAlert(Constants.RouteView.locationAlertTitle,
                             body: Constants.RouteView.locationAlertMessage,
                             defaultButton: true,
                             cancelButtonTitle: Constants.RouteView.acceptTitle)
            case .authorizedAlways, .authorizedWhenInUse:
                    if drawingStatus == StatusRoute.drawingIsStopped {
                        guard let currentLocation = locationManager.location else {
                            return
                        }
                        drawingStatus = .drawingIsRunning
                        setupInitialPositionMarket(location: currentLocation)
                        presenter?.timeStarted = Date()
                        locationManager.startUpdatingLocation()
                    } else {
                        drawingStatus = .drawingIsStopped
                        presenter?.setTrackingTime()

                        //Calculate distance
                        let distanceInMt = GMSGeometryLength(path)
                        presenter?.routeModel.distance = Double(distanceInMt)

                        //Get map image
                        let image = mapView.asImage()
                        presenter?.routeModel.image = image.pngData()
                        locationManager.stopUpdatingLocation()

                        //Show view to save route
                        confirmView()
                    }
                    styleButton()
            @unknown default:
                break
            }
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }

    func saveRoute() {
        if let name = nameTextField?.text, !name.isEmpty {
            presenter?.routeModel.name = name
        } else {
            presenter?.routeModel.name = Constants.RouteView.defaultNameRoute
        }
        presenter?.saveRoute()
    }

    func updateCurrentPositionMarker(currentLocation: CLLocation) {
        self.currentPositionMarker.map = nil
        self.currentPositionMarker = GMSMarker(position: currentLocation.coordinate)
        self.currentPositionMarker.icon = GMSMarker.markerImage(with: UIColor.cyan)
        self.currentPositionMarker.map = self.mapView

        DispatchQueue.main.async
        {
            let bounds = GMSCoordinateBounds(path: self.path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        }
    }

    func setupInitialPositionMarket(location: CLLocation) {
        //Initial position
        self.initialPositionMarker.map = nil
        self.initialPositionMarker = GMSMarker(position: location.coordinate)
        self.initialPositionMarker.icon = GMSMarker.markerImage(with: UIColor.red)
        self.initialPositionMarker.map = self.mapView
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 17.0)
        self.mapView?.animate(to: camera)
    }

    func updateOverlay(currentPosition: CLLocation) {
        self.path.add(currentPosition.coordinate)
        self.polyline.path = self.path
    }

    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        self.mapView.animate(to: camera)
    }
}

extension RouteViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        if drawingStatus == .drawingIsRunning {
            self.updateOverlay(currentPosition: location)
            self.updateMapFrame(newLocation: location, zoom: 17.0)
            self.updateCurrentPositionMarker(currentLocation: location)
        } else {
            setupInitialPositionMarket(location: location)
            locationManager.stopUpdatingLocation()
        }

    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("error:\(error.localizedDescription)")
    }
}

extension RouteViewController: RouteViewProtocol {

    func clearRoute() {
        drawingStatus = .drawingIsStopped
        self.currentPositionMarker.map = nil
        self.path.removeAllCoordinates()
        mapView.clear()
        polyline.path = self.path
        setupPolylineAnnotation()
        locationManager.startUpdatingLocation()
    }
    
}
