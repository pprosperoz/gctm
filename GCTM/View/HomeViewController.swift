//
//  HomeViewController.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/22/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var container: UIView!

    private lazy var routeViewController: RouteViewController? = {
        guard let viewController = getViewController(viewController: RouteViewController.self, storyboardName: "Main") else {
            return nil
        }
        self.add(asChildViewController: viewController)
        return viewController
    }()

    private lazy var routesViewController: ListRoutesViewController? = {
        guard let viewController = getViewController(viewController: ListRoutesViewController.self, storyboardName: "Main") else {
            return nil
        }
        self.add(asChildViewController: viewController)
        return viewController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        self.title = Constants.HomeView.title
    }

    private func add(asChildViewController viewController: UIViewController?) {
        guard let  viewController = viewController else {
            return
        }
        addChild(viewController)
        container.addSubview(viewController.view)
        viewController.view.frame = container.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }

    private func remove(asChildViewController viewController: UIViewController?) {
        guard let  viewController = viewController else {
            return
        }
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }

    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            routeViewController?.removeFromParent()
            remove(asChildViewController: routesViewController)
            add(asChildViewController: routeViewController)
        } else {
            remove(asChildViewController: routeViewController)
            add(asChildViewController: routesViewController)
        }
    }

    @IBAction func changeViewAction(_ sender: UISegmentedControl) {
        updateView()
    }
}
