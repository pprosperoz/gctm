//
//  DetailsRouteViewController.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/22/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import UIKit

class DetailsRouteViewController: UIViewController, DetailsViewProtocol {


    @IBOutlet weak var nameTextField: UILabel!
    @IBOutlet weak var distanceTextField: UILabel!
    @IBOutlet weak var timeTextField: UILabel!
    @IBOutlet weak var routeImageView: UIImageView!
    private var presenter: DetailsPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    func setPresenter(_ presenter: DetailsPresenterProtocol) {
        self.presenter = presenter
    }
    
    func setupView() {
        self.addBackButton()
        self.addDetailsButton(action: #selector(self.shareOptions))
        guard let name = presenter?.details?.nameRoute,
            let distance = presenter?.details?.distance,
            let time = presenter?.details?.time,
            let image = UIImage(data: presenter?.details?.routeImg ?? Data()) else {
            return
        }

        timeTextField.sizeToFit()
        distanceTextField.sizeToFit()

        self.title = Constants.DetailsView.title
        self.nameTextField.text = name
        let distanceInkm: Double = distance / 1000
        self.distanceTextField.text = Constants.DetailsView.distanceText + " \(distanceInkm.toString())"
        self.timeTextField.text = Constants.DetailsView.timeTravelText + " \(time)"
        self.routeImageView.image = image

    }

    @objc private func shareOptions() {
        let imageToSend = view.asImage()
        let viewController = UIActivityViewController(activityItems: [imageToSend], applicationActivities: nil)
        viewController.popoverPresentationController?.sourceView = self.view
        viewController.excludedActivityTypes = [.saveToCameraRoll, .airDrop, .addToReadingList, .mail, .addToReadingList, .assignToContact, .copyToPasteboard, .markupAsPDF, .openInIBooks, .postToFlickr, .postToTencentWeibo, .postToVimeo, .postToTwitter, .postToWeibo, .print, .init(rawValue: "com.apple.reminders.sharingextension"), .init(rawValue: "com.apple.mobilenotes.SharingExtension"), .init(rawValue: "com.linkedin.LinkedIn.ShareExtension")]
        present(viewController, animated: true, completion: nil)
    }
}
