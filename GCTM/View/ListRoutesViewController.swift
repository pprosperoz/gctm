//
//  ListRoutesViewController.swift
//  GCTM
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/22/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import UIKit

class ListRoutesViewController: UITableViewController {

    private var presenter: ListRoutesPresenterProtocol?
    
    override func viewDidLoad() {
        presenter = ListRoutesPresenter(view: self)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.estimatedRowHeight = 100
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfRows ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "routeCell", for: indexPath)

        guard let routeCell = presenter?.configureCell(index: indexPath.row, cell: cell) else {
            return UITableViewCell()
        }
        return routeCell
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let info = presenter?.cellInformation(index: indexPath.row), let id = info.id else {
                return
            }
            CoreDataHelper.shared.delete(id: id, completion: {(success,message) in
                if success {
                     tableView.reloadData()
                }

            })
        } else if editingStyle == .insert {

        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let info = presenter?.cellInformation(index: indexPath.row) else {
            return
        }
        guard let detailsVC = getViewController(viewController: DetailsRouteViewController.self, storyboardName: "Main") else {
            return
        }
        let presenter = DetailsPresenter(details: info, view: detailsVC)
        detailsVC.setPresenter(presenter)

        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}


extension ListRoutesViewController: ListRoutesViewProtocol {

    func updateView() {
        self.tableView.reloadData()
    }

}
